console.log("Hello World");

// [ SECTION ] Javascript Synchronous vs. Asynchronous
	// Javascript is by default a synchronous language, meaning that only one statement is execyted at a time

// This can be proven when a statement has an error, JS will not proceed with the next statement
	console.log('Hellow World')
	// consoel.log('Hellow Again World')
	console.log('Again World Hellow ')

	for(let i = 0; i <= 1500; i++){
		console.log(1);
	};

	console.log("I,m 1501")

	// Asynchronous means that we can proceed to execute other statements, while time consuming code is running in the background

	// [SECTION ] Getting All Post

	// The FETCH API allows us to asynchronously request for a resources(data) 
	// A "promise" is an object that represents the eventual completion (or failure) of an asynchronous function and it's resulting value

	// Syntax: 
		// fetch("URL") 
	console.log(fetch('https://jsonplaceholder.typicode.com/posts'));

	// Syntax:
		// fetch("URL")
		// .then((response) => {})

	// Retrieves all posts following the REST API (retrieve, /posts, GET)
	// By using the then method we can now check for the status of the promise

		fetch('https://jsonplaceholder.typicode.com/posts')
		.then(response => console.log(response.status));

		// The "fetch" method will return a "promise" that resolves to a "response" object
		// The 'then' method captures the "response" object and returns another "promise" which will eventually be "resolved" or "rejected"

	fetch('https://jsonplaceholder.typicode.com/posts')
		// Use the JSON method fromn the response object to convert data retrieved into json format to be used in our application
	.then((response) => response.json())
		// Print the converted JSON value from the "fetch" request
		// Using multiple "then" methods creates a "promise chain"
	.then((json) => console.log(json));

	// The "async" and "await" keywords is another approach that can be used to achieve asynchronous code
	// Used in function to indicate which portion of the code should be waited for 
	// Creates an asynchronous function

	async function fetchData(){

		let result = await fetch("https://jsonplaceholder.typicode.com/posts")

		console.log(result);

		let json = await result.json();

		console.log(json);

	}

	fetchData();

	console.log("Running");

// [ SECTION ] Getting a specific post
		// Retrieves a specific post following the REST API (retrieve, /post/:id, GET)

	fetch("https://jsonplaceholder.typicode.com/posts/1")
	.then((response) => response.json())
	.then((json) => console.log(json));

// [ SECTION ] Creating a Post

	// Syntax:
		// fetch("URL", options)
		// .then((response) => {})
		// .then((response) => {})

	// Creates a new post following the REST API (creates, /post/:id, POST)

	fetch("https://jsonplaceholder.typicode.com/posts", {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			title: "New Post",
			body: "Hellow World?!",
			userID: 1,
		})

	})
	.then((response) => response.json())
	.then((json) => console.log(json));

// [ SECTION ] Updating a post using PUT method
	// Updates a specific post following the REST API (update, /posts/:id, PUT)

	fetch("https://jsonplaceholder.typicode.com/posts/1", {
		method: 'PUT',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			title: "Updated Post",
			body: "Hellow Again World?!",
			userID: 1,
		})

	})
	.then((response) => response.json())
	.then((json) => console.log(json));

// [ SECTION ] Updating a post using PATCH method
	// Updates a specific post following a REST API (update, /posts/:id, PATCH)

	fetch("https://jsonplaceholder.typicode.com/posts/1", {
		method: 'PATCH',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			title: "Corrected Post"
		})

	})
	.then((response) => response.json())
	.then((json) => console.log(json));

	// Difference between PUT and PATCH
	// PATCH method is used to partially update an existing resource while PUT is used to completely replace an existing resource or create a new resource 
	// PATCH is used to update a single/several properties 
	// PUT is used for the whole object

// [ SECTION ] Deleting a post
	// Deleting a specific post following the REST API (delete, /post/:id, DELETE)

	fetch("https://jsonplaceholder.typicode.com/posts/1", {
		method: 'DELETE'
	});

// [ SECTION ] Retrieving nested/related comments to the posts
	// Retrieving comments for a specific post following the REST API (retrieve, /posts/:id/comments, GET)

	fetch("https://jsonplaceholder.typicode.com/posts/1/comments", {
		method: 'GET'
	})
	.then((response) => response.json())
	.then((json) => console.log(json))