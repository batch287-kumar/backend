// Fetch request to retrieve all to-do list items:
fetch('https://jsonplaceholder.typicode.com/todos')
.then((response) => response.json())
.then((json) => console.log(json));


// create an array using the map method to return just the title of every item:
fetch('https://jsonplaceholder.typicode.com/todos')
.then(response => response.json())
.then(data => {
  const titles = data.map(item => item.title);
  console.log(titles);
});


// Fetch request to retrieve a single to-do list item:
fetch('https://jsonplaceholder.typicode.com/todos/1')
.then(response => response.json())
.then(data => {
  console.log(`Title: ${data.title}`);
  console.log(`Status: ${data.completed ? 'Completed' : 'Incomplete'}`);
});


// Fetch request to create a to-do list item using the POST method:
fetch('https://jsonplaceholder.typicode.com/todos', {
  method: 'POST',
  headers: {
    'Content-type': 'application/json',
  },
  body: JSON.stringify({
    title: 'New task',
    completed: false
  })
})
.then(response => response.json())
.then(json => console.log('Created item using POST:', json));


// Fetch request to create a to-do list item using the PUT method:
fetch('https://jsonplaceholder.typicode.com/todos/1', {
  method: 'PUT',
  headers: {
    'Content-type': 'application/json',
  },
  body: JSON.stringify({
    title: 'Updated task',
    completed: true
  })
})
.then(response => response.json())
.then(json => console.log('Created item using PUT:', json));


// Update a to-do list item by changing the data structure:
fetch('https://jsonplaceholder.typicode.com/todos/1', {
  method: 'PUT',
  headers: {
    'Content-type': 'application/json',
  },
  body: JSON.stringify({
    title: 'Updated task',
    description: 'Task description',
    status: 'in progress',
    dateCompleted: '2023-07-06',
    userId: 1
  })
})
.then(response => response.json())
.then(json => console.log('Data Structure updated item:', json));


// Update a to-do list item by changing the status and adding a date using PATCH:
fetch('https://jsonplaceholder.typicode.com/todos/1', {
  method: 'PATCH',
  headers: {
    'Content-type': 'application/json',
  },
  body: JSON.stringify({
    completed: true,
    completedDate: '2023-07-06'
  })
})
.then(response => response.json())
.then(json => console.log('Partially updated item:', json));


// Fetch request using the DELETE method
fetch('https://jsonplaceholder.typicode.com/todos/1', {
  method: 'DELETE',
})
.then(response => {
  if (response.ok) {
    console.log('Item deleted successfully');
  } else {
    console.log('Failed to delete item');
  }
});
