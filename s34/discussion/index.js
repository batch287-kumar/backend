                                                                                                // Use the require directive to load the express module/package
// It allows us to access to methods and functions that will alllow us to easily create a server
const express = require("express");

// Creates an application using express
// In layman's term, app is our server
const app = express();

// For our application server to run, we need a port to listen to
const port = 3001;

// Use middleware to allow express to read JSON
app.use(express.json())

//Use middleware to allow express to able to read more data types from a response
app.use(express.urlencoded({extended:false}));

// [ SECTION ] Routes
	// Express has methods corresponding to each HTTP method

app.get("/hello", (request, response) => {
	response.send("Hellow from the /hello endpoint!");

}) 

app.post("/display-name", (req, res) => {
	console.log(req.body);
	res.send(`Hellow there ${req.body.firstName} ${req.body.lastName}!`)
})

// Sign-up user in our Users

let users = []; // This is our mock database

		/*
			app.post("/signup", (request, response) => {
			  const { username, password } = request.body;

			  if (username && password) {
			    users.push({ username, password });
			    response.send("User signed up successfully!");
			  } else {
			    response.send("Username and password are required.");
			  }
			});
		*/

app.post("/signup", (request, response) => {
	// Mini-Activity: If our body.password and body.username is empty don't include that in our mock database thus if it's not empty add the username and password in our mock database

	console.log(request.body)
	// If contents of the "request body" with the property "username" and "password" is not empty
	if (request.body.username && request.body.password) {

		// This will store the user object sent via Postman to the users array created above
		users.push(request.body);

		// Recognise the success of the request
		response.send("User signed up successfully!");
		console.log(users);
	} else {
		response.send("Username and password are required.");
	}
}); 

// This route expects to recieve a put request at the URI "/change-password"

app.put("/change-password", (req, res) => {

	let message;
	
	// Creates a for loop that will loop through the elements of the "users" array
	for(let i = 0; i < users.length; i++){

		// If the username provided in the client/Postman and the usertname of the current object in the loop is same, run the following code
		if(req.body.username == users[i].username){

			// Chnages the users[i].password value to the req.body.password
			users[i].password = req.body.password;

			// Changes the message to be sent back by the response 
			message = `User ${req.body.username}'s password has been updated.`

			console.log("Newly updated mock database:")
			console.log(users)

			// Breaks out of the loop once a user that matches teh username provided in the client/Postman is found
			break;

		// If no user was found
		}else{

			// Changes the message to be sent back by the response
			message = "User does not exist."
		}
	}
	
	// Send a response back to the client/Postman once the password has been updated or if a user is not found
	res.send(message);
})

app.listen(port, () => console.log(`Server is currently running at port ${port}`))
