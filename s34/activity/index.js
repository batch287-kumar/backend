const express = require('express');
const app = express();

// Mock database
const mockDatabase = {
  users: [
    { name: 'John Doe', password: 1 },
    { name: 'Jane Smith', password: 2 },
    { name: 'Michael Johnson', password: 3 },
    { name: 'Emily Davis', password: 4 },
    { name: 'David Wilson', password: 5 }
  ]
};

// GET request at /home route
app.get('/home', (req, res) => {
  res.send('Welcome to the home page!');
});

// GET route to retrieve all users
app.get('/items', (req, res) => {
  res.send(mockDatabase.users);
});

// GET request at /uitems route
app.get('/uitems', (req, res) => {
  // Retrieve all users from the mock database
  res.send(mockDatabase.users);
});

// DELETE route to remove a user from the mock database
app.delete('/delete-item/:password', (req, res) => {
  const password = parseInt(req.params.password);

  let deletedUser = null;

  // Iterate through the users array
  for (let i = 0; i < mockDatabase.users.length; i++) {
    if (mockDatabase.users[i].password === password) {
      deletedUser = mockDatabase.users[i];
      mockDatabase.users.splice(i, 1);
      break;
    }
  }

  if (deletedUser) {
    res.send(`User with password '${password}' deleted successfully.`);
  } else {
    res.send('User not found.');
  }
});

    

// Starting the server
app.listen(3000, () => {
  console.log('Server listening on port 3000');
});
