	// 1. Create variables to store to the following user details:

	let firstName = "John";
	let lastName = "Smith";
	let age = "30";
	let hobbies = ["Biking", "Mountain Climbing", "Swimming"];	
	let workAddress = {
			houseNumber: '32',
			street: 'washington',
			city: 'Lincoln',
			state: 'Nebrasha',
		}
	console.log("First Name: " + firstName);
	console.log("Last Name: " + lastName);
	console.log("Age: " + age);
	console.log("Hobbies:");
	console.log(hobbies);
	console.log("Work Address:");
	console.log(workAddress);





			
	// 2. Debugging Practice - Identify and implement the best practices of creating and using variables by avoiding errors and debugging the following codes:

	let fullName = "Steve Rogers";
	console.log("My full name is " + fullName);

	let age2 = "40";
	console.log("My current age is: " + age2);
	
	let friends = ["Tony","Bruce","Thor","Natasha","Clint","Nick"];
	console.log("My Friends are: ")
	console.log(friends);

	let profile = {
		username: "captain_america",
		fullName: "Steve Rogers",
		age: "40",
		isActive: false,
	}
	console.log("My Full Profile: ")
	console.log(profile);

	let fullName2 = "Bucky Barnes";
	console.log("My bestfriend is: " + fullName2);

	let lastLocation = "Atlantic Ocean";
	lastLocation = "Arctic Ocean";
	console.log("I was found frozen in: " + lastLocation);