console.log("Hellow world!");

// Conditional Statements 
	// Allows us to control the flow of our program. It also allows us to run a statement/instruction if a condition is met or run another separate instruction if otherwise.

//[ SECTION ] If, Else If, and Else Statement

	let numA = -1;

		// If Statements

		if(numA < 0){
			console.log("Hellow");
		};

	// If numA is less than 0, run console.log("Hellow");

	/*
	Syntax:

		if(condition){
			statement;
		};

	*/

	console.log(numA <= 0); // results will be true.

	// The result of expression added will be added in the if's condition must result to true, else the statement inside the if() will not run.

	// let's update the variable and run as if statement with the same condition

	numA = -1;

		if(numA < 0){
			console.log("Hellow again from numA is -1");
		};

		// This will not run because the expression now results to false:
		console.log(numA > 0);

		let city = "New York";

		if(city == "New York"){
			console.log("Welcome to New York City!");
		};

		console.log(city == "New york");

	// Else If Clause

		// Executes a statement if previous conditions are false and if the specified condition is true

		let numH = 1;

		numA = 1;

		if(numA < 0){ // The condition is now false, if false, code block will not run
			console.log("Hellow");
		} else if( numH > 0){ // The condition now here is true, thus this will print console.log("Wurld?")
			console.log("Wurld?");;
		};

		// We were able to run the elseif() statement after we evaluated that the if condition was failed.

		// If the if() condition was passed and run, we will no longer evaluate to elseif() and end the process there.

		let numB = 5;

		if(numB < 0){
			console.log("This will not run!");
		} else if (numB < 5){
			console.log("The value is less than 5");
		} else if (numB < 10){
			console.log("The value is less than 10");
		};

		// Let's update the city vartiable and look at another example

		city = "Tokyo";

		if(city === "New York"){
			console.log("Welcome to New York!");
		} else if(city === "Tokyo"){
			console.log("Welcomne to Tokyo, Japan!");
		};

	// Else Satement

		// Executes a statement if all existing condition are false

		console.log(numA);

		if(numA < 0){
			console.log("Hello!");
		} else if(numH === 1){
			console.log('World');
		} else {
			console.log("Again!");
		};

		// Since both the preceding if and else if conditions failed, the else statemennt was run

		numB = 21;

		if(numB < 0){
			console.log("This will not run!");
		} else if (numB < 5){
			console.log("The value is less than 5.");
		} else if (numB < 10){
			console.log("The value is less than 10.");
		} else if (numB < 15){
			console.log("The value is less than 15.");
		} else if (numB < 20){
			console.log("The value is less than 20.");
		} else {
			console.log("The value is greater than 20!");
		};

		// else {
		// 	console.log("Do you think this will run?");
		// };

		// In fact, it results to an error.

		// else if(numH === 0){
		// 	console.log("World");
		// } else {
		// 	console.log("How about this one?");
		// };

		// Same goes for else if, there should be a preceding if() first.

	// If, Else If, and Else Statements with Functions

		// Most of the times we would like to use if, else if, and else statement with functions to control the flow of our application.

		let message = 'No Message';
		console.log(message);

		function determineTyphoonIntensity(windSpeed){
			if(windSpeed < 30){
				return 'Not a typhoon yet.';
			} else {
				return 'Typhoon detected! Keep Safe!';
			};
		};

		message = determineTyphoonIntensity(25);
		console.log(message);

		// MiniActivity

		// WindSpeed less than or equal to 61 = Tropical Depression Detected
		// WindSpeed less than or equal to 62 and windspeed is less than or equal to 88 = Tropical Storm Detected
		// WindSpeed is grester tah or equal to 89 or WindSpeed is less than or equal to 117 = Severe Tropical Storm Detected
		// WindSpeed is more than 117 = Typhoon Detected

		function determineTyphoonIntensity2(WindSpeed){
			if(WindSpeed < 30){
				return 'Not a typhoon yet.';
			} else if(WindSpeed <= 61){
				return 'Tropical Depression Detected!';
			} else if(WindSpeed <= 88){
				return 'Tropical Storm Detected';
			} else if(WindSpeed <= 117){
				return 'Severe Tropical Storm Detected';
			} else {
				return 'Typhoon Detected';
			};
		};

		message2 = determineTyphoonIntensity2(188);
		console.log(message2);

	// Truthy and Falsy

		// In JS a truthy is a value taht is considered true when encounteredin a boolean context.

		/*
		Falsy Values/Exception for truthy:
			1. False
			2. 0
			3. -0
			4. ""
			5. null
			6. undefined
			7. Nan
		*/

		// True Examples
		if(true){
			console.log("Truthy!");
		};
		if(1){
			console.log("Truthy!");
		};
		if([]){
			console.log("Truthy!");
		};

		// False Examples
		if(false){
			console.log("Falsy!");
		};
		if(0){
			console.log("Falsy!");
		};
		if(undefined){
			console.log("Falsy!");
		};

	// Conditional (Ternary) Operator

		// The conditional Ternary Operator takes in three operands:
			// 1.Condition
			// 2. Expression to execute if the condition is truthy
			// 3. Expression to execute if the condition is falsy
		// Can be used as an alternative to an "if else" statement

		/*
		Syntax:
			(expression) ? ifTrue : ifFalse;
		*/

	// Single Ternary Statement Execution
		let ternaryResult = (1 < 18) ? false  : true; 
		// let ternaryResult = (1 < 18) ? false  : true; // Will result to false
		console.log("Result of the Ternary Operator: " + ternaryResult);

	// Multiple Statement Execution

		let name;

		function isOfLegalAge(){
			name = "John";
			return 'You are of the legal age limit';
		};

		function isUnderAge(){
			name = 'Jane';
			return 'You are under the age limit';
		};

		let age = parseInt(prompt('What is your age?'));
		console.log(age);
		let legalAge = (age > 18) ? isOfLegalAge() : isUnderAge();
		console.log("Result of Ternary Operator in functions: " + legalAge + ", " + name);

	// Switch Statement 

		// The switch statement evaluates an expression and matches the expression's value to a case clause
		// The ".toLowerCase()" is a function/method thatw ill change the input recieved from the prompt into all lowercase 

		/*
		Syntax:
			switch(expression){
				case value;
					statement;
					break;
				default
					statement;
					break;
			}
		*/

		let day = prompt('What day of the week is it today?').toLowerCase();

		console.log(day);

		switch(day){
			case 'monday':
				console.log('The color of the day is red!');
				break;
			case 'tuesday':
				console.log('The color of the day is blue!');
				break;
			case 'wednesday':
				console.log('The color of the day is orange!');
				break;
			case 'thursday':
				console.log('The color of the day is violet!');
				break;
			case 'friday':
				console.log('The color of the day is yellow!');
				break;
			case 'sunday':
				console.log('The color of the day is brown!');
				break;
			case 'saturday':
				console.log('The color of the day is pink!');
				break;
			default:
				console.log('Please input a valid Day');
				break;
		};

	// Try-Catch-Finally Statement

		// try-catch statements are commonly used for error handling

		function showIntensityAlert(windSpeed){
			// Insert try-catch-finally statement
			try {
				alerat(determineTyphoonIntensity(windSpeed));
			} catch(error) {
				console.log(typeof error);
				console.log(error);
				console.warn(error.message);
			} finally {
				alert("Intensity alert will show new alert!");
				console.log('This is from finally!')

				// Continue execution of code regardless of success and failure of code executionin the "try" block to handle/resolve errors
			}
		}

		showIntensityAlert(56);