function friends(friends_region1, friends_region2){
	this.hoenn = friends_region1,
	this.kanto = friends_region2
};

function trainer(name, age, pokemons, friends_region1, friends_region2){
	this.name = name,
	this.age = age,
	this.pokemon = pokemons,
	this.friends = new friends(friends_region1, friends_region2),
	this.talk = function(){
		console.log(this.pokemon[0] + "! I choose you!");
	}
};

let ash = new trainer('Ash Ketchum', 10, ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"], ["May", "Max"], ["Brock", "Misty"]);
console.log(ash);

console.log("Result of dot notation:");
console.log(ash.name);

console.log("Result of square bracket notation:");
console.log(ash.pokemon);

console.log("result of talk method:");
ash.talk();


function Pokemon(name, level){

	// Properties
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = level;

	// Methods
	this.tackle = function(target){
		console.log(this.name + " tackled " + target.name);
		target.health -= this.attack;
		console.log(target.name + "'s health is now reduced to " + target.health);
		if(target.health <= 0){
			target.faint();
		}
	};
	this.faint = function(){
		console.log(this.name + ' fainted ');
	};
};

	let pikachu = new Pokemon("Pikachu", 12);
	let geodude = new Pokemon("Geodude", 8);
	let mewtwo = new Pokemon("Mewtwo", 100);


	geodude.tackle(pikachu);

	console.log(pikachu);

	mewtwo.tackle(geodude);