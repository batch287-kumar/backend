console.log("Hello World!");


// [ SECTION ] Objects
	// An object is a data type that is used to represent real world objects 
	// Information stored in objects are represented in a "key:value" pair

	// Creating object using object initializers/literal notation

	// Syntax:
		// let objectName = {
			// keyA: valueA,
			// keyB: valueB
		//}

	let cellphone = {
		name : 'Nokia 3210',
		manufactureDate: 1999
	};

	console.log('Creating Object using object initializers/literal notation:');
	console.log(cellphone);
	console.log(typeof cellphone);

	// Creating an object using a constructor function

		// Creates a reusable function to create several object that have the same data structure

		// Syntax:
			// function objectName(keyA, keyB){
				// this.keyA = keyA,
				// this.keyB = keyB
			//}

		// This is an object
		// The "this" keyword allows us to assign a new object's properties by associating them with the values recieveed from a constructor function's parameters

	function Shirt(color, day){
		this.colorSelected = color;
		this.daySelected = day;
	};

	// The "new" operator creates an instance of an object

	let shirt = new Shirt('Red', "Monday");
	console.log('Result of creating object using a constyructors: ');
	console.log(shirt);

	let dress = new Shirt('Blue', "Tuesday");
	console.log('Result of creating object using a constyructors: ');
	console.log(dress);

	let socks = Shirt('yellow', "Thursday");
	console.log('Result of creating object using a constyructors: ');
	console.log(socks);

		// Unable call that new operators it will return unddefined without the 'new' operator
		// The behaviour for this is is like calling/invoking the Laptop function instead of creating a new object instance.

	// Create empty objects
	let commputer = {};
	let myComputer = new Object();

	// [ SECTION ] Accessing Object Properties 

	// Using the dot notation
	console.log('Result from dot notation: ' + shirt.colorSelected);

	// Using the square bracket notation
	console.log('Result from square bracket notation: ' + shirt['daySelected'])

	// Accessing Array Objects

		// Accessing array elements can be also done using square brackets

	let array = [ shirt, dress ];
	console.log(array);
	console.log(array[0]['daySelected']);
	// This tells us that array[0] ia an object by using the dot notation
	console.log(array[0].daySelected);

	// Initializing/Adiing object properties using dot notation

	let car = {};

	car.name = 'Honda Civic';
	console.log("Result from adding object using the dot notation");
	console.log(car);

	// Initializing/Adiing object properties using bracket notation

	car['manufacture date'] = 2023;
	console.log(car['manufacture date']);
	console.log(car['manufacture Date']);
	console.log(car.manufacturedate);
	console.log("Result from adding object using the bracket notation");
	console.log(car);

	// Delete object properties 
	delete car['manufacture date'];
	console.log('Result form deleting properties');
	console.log(car);

	// Reassigning object properies
	car.name = 'Dodge Charger R/T';
	console.log('Result from reassigning properties');
	console.log(car);

// [ SECTION ] Object Methods
	// A method is a function which is a property of an object

	let person = {
		name: 'John',
		talk: function(){
			console.log('Hello my name is ' + this.name);
		}
	};

	console.log(person);
	console.log('Result from object method: ');
	person.talk();

	// Methods are useful for creating reusable functions that perform tasks related to object

	let friend = {
		firstName: 'Joe',
		lastName: 'Smith',
		address: {
			city: "Austin",
			country: 'Texas'
		},
		email: ['joe@mail.com', 'joesmith@mail.xyz'],
		introduce: function(){
			console.log("Hello my name is " + this.firstName + " " + this.lastName);
		},
		introduceAddress: function(){
			console.log("My address is " + this.address.city + ", " + this.address.country);
		},
		introduceContactDetails: function(){
			console.log("You can email me on: " + this.email);
		}
	};

	friend.introduce();
	friend.introduceAddress();
	friend.introduceContactDetails();

	// Mini-Activity,
	// Generate a function introduceAddress, that basically inform Joe's address
	// Generate a function introduceContactDetails, that basically inform Joe's contact details
	// Trigger both function

	// [ SECTION ] Real World Application of Objects

		/*
		Scenario
			1. We would like to create a game that would have several pokemon interact with each other
			2. Every pokemon nwould have the same set as stats, properties and functions
		*/

	// Using objects literals to create multiple kinds of pokemon would be time consuming

	let myPokemon = {
		name: 'Pikachu',
		level: 3,
		health: 100,
		attack: 50,
		tackle: function(){
			console.log("This Pokemon tackled targetPokemon");
			console.log("targetPokemon's health is now reduced to -targetPokemonHealth_");
		},
		faint: function(){
			console.log("Pokemon Fainted");
		}
	}

	console.log(myPokemon);

	function Pokemon(name, level){

		// Properties
		this.name = name;
		this.level = level;
		this.health = 2 * level;
		this.attack = level;

		// Methods
		this.tackle = function(target){
			console.log(this.name + " tackled " + target.name);
			console.log("targetPokemon's health is now reduced to _targetPokemonHealth_");
		};
		this.faint = function(){
			console.log(this.name + ' fainted ');
		};
	};

	let pikachu = new Pokemon("Pikachu", 16);
	let rattata = new Pokemon("Rattata", 8);

	pikachu.tackle(rattata);
	rattata.faint();