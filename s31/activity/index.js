const http = require('http');

const port = 3000;

const server = http.createServer((request, response) => {
  console.log(`Server is running on port ${port}`);
  if (request.url == '/login') {
    response.writeHead(200, {'Content-Type': 'text/plain'})
    response.end('You are at the login page')
  }
  else {
    response.writeHead(404, {'Content-Type': 'text/plain'})
    response.end('Error: Page not found')
  }
})

server.listen(port);

console.log(`Server is successfully running at localhost: ${port}`);