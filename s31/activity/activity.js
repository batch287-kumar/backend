// a. What directive is used by Node.js in loading the modules it needs?
// Answer: The `require` directive is used by Node.js to load the modules it needs.

// b. What Node.js module contains a method for server creation?
// Answer: The `http` module in Node.js contains a method for server creation.

// c. What is the method of the http object responsible for creating a server using Node.js?
// Answer: The `createServer` method of the `http` object is responsible for creating a server using Node.js.

// d. What method of the response object allows us to set status codes and content types?
// Answer: The `writeHead` method of the response object allows us to set status codes and content types.

// e. Where will console.log() output its contents when run in Node.js?
// Answer: When `console.log()` is run in Node.js, its contents will be output to the console or terminal where the Node.js application is being executed.

// f. What property of the request object contains the address' endpoint?
// Answer: The `request.url` property of the request object contains the address' endpoint.