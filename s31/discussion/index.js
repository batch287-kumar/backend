// Use the "require" directive to load Node.js modules
// A module is a software component or part of a program that contains one or more routine
// The "http module" lets Node.js transfer data using the Hyper Text Transfer Protocol
// The "http module" is a set of individual files that contain code to create a "component" that helps establish data transfer between the applications
// HTTP is a protocol that allows the fetching of resources such as HTML documents
// Client/browser and servers (node.js/express,js application) communicate by exchanging individual messages.
	// The messages sent by the 'client', usually a web browser is called "requests"
	// The messages sent by the 'server' as an answer are caleld "responses"

const http = require("http");

// Using this module's createServer() method, we can create an HTTP server that listens to request on a specified port and gives a response back to the Clients 
//The http module has a createServer() method that accepts a function with an argument and allows for the creation of a server
// The arguments passed in the function are request and response objects (data type) that contains methods that allows us to recieve request from the client and send responses back to it.
http.createServer(function (request, response){

	// Use the writeHead() method to: 
	// Set a status code for the response - a 200 means OK
	// Set the content-type of the response as a plain text message
	response.writeHead(200, {'Content-Type': 'text/plain'});

	// Send the response with the text content "Hello World?";
	response.end('Hello World?');

// A port is a virtual point where network connection start and end.
// each port is associated with a specific process or service
// The server will be assigned to port 4000 via the "listen(4000)" method where the server will listen to any request that are sent to it eventually communicating with our server.
}).listen(4001)

// When server is running, console will print the message
console.log('Server is running at localhost:4001');