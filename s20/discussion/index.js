console.log("Hello World!");

// [ SECTION ] While Loops

	// A while lopps takes in an express/condition
	// If condition evaluates to true, the statement in the code block will run.

	/*
	Syntax:
		while(expression/condition){
			statement;
		};
	*/

	let count = 5;
	function whileLoop(){
		// While the value of count is not equal to 0
		while(count != 0){

			// The current value of count is printed out.
			console.log("While: " + count);

			// Infinite loop, our program being written without the end
			// Make sure taht exp[ression/condition in loops have theirn corresponding increment/decrement operators to stop the loop.
			count--;

			// Note:
			// Make sure that the corresponding increment/decrement is relevant to the condition/expression
		};
	};


	// Another example

	function gradelevel(){
		let grade = 1;

		while(grade <= 5){
			consoile.log("I am a grade " + grade + " student!");

			grade++;
		};
	};

// [ SECTION ] Do While Loop

	// A do-while loop works alot like a while loop, but unlike while loops, do-while loops guarantee that the code will be executed at least once.

	/*
	Syntax:
		do {
			statement;
		} while(expression/condition)
	*/ 
	function doWhile(){
		let count = 20;

		do {
			console.log("What ever happens, I will be here");
			count--;
		} while (count > 0);
	};

	function secondDoWhile(){
		let number = Number(prompt('Give me a number'));

		do {

			// The current value of number of printed.
			console.log("Do while: " + number);

			//Increase the vlaue of the number by 1 after every iteration to stop the loop when it reaches the 10 or greater 
			// number = number + 1
			number += 1;

			// Provide a number of 10 or greater will run the code block once and will stop the loop.
		} while(number < 10);
	};

// [ SECTION ] For Loop

	// A for loop is more flexible than the while or do while loop.
	// For loop is consist of three parts:
		// 1. The "initialization" value that will track the progression of the loop.
		// 2. The "expression/condition" that will be evaluated which will determine whether the loop will run one more time or not
		// 3. The "finalExpression" indicates how to advances teh loop/how the variable will behave.

	/*
	Syntax:
		for(initialization; expression/condition; finalExpression) {
			statement;
		}
	*/ 
	function forLoops(){
		for(let count = 0; count <= 20; count++) {
			console.log("You are currently: " + count);
		};
	};

	// [ SUB-SECTION ] Loops for letters

	let myString = "Alex";
	console.log(myString);
	console.log(myString.length);

		console.log(myString[0]);
		console.log(myString[1]);
		console.log(myString[2]);
		console.log(myString[3]);
		console.log(myString[4]);

		let x = 0;

		// We create a loop that will print out the individual letters of the myString variable
		for(x; x < myString.length; x++){

			// The current variable of myString is printed out using it's index value.
			console.log(myString[x]);
		};

	// Mini-Activity 
		// Create variable taht contains your name 
		// Foir every vowel in your name it should be replace with 0

	function changeName(){
		function replaceVowelsWithZero(myName) {
		    let myNewName = '';

		    for (let i = 0; i < myName.length; i++) {
		        if (myName[i].toLowerCase() === 'a' || 
		        	myName[i].toLowerCase() === 'e' || 
		        	myName[i].toLowerCase() === 'i' || 
		        	myName[i].toLowerCase() === 'o' || 
		        	myName[i].toLowerCase() === 'u')
		        	myNewName += '0';
		        else
		       		myNewName += myName[i];
		    }
		    return myNewName;
		};

		let myName = prompt("Enter your name: ");
		console.log("My name is:" + myName);
		let myNewName = replaceVowelsWithZero(myName);
		console.log("My new name is:" + myNewName);
	};

	// changeName();

// [ SECTION ] Continue and Break Statements

	// the "continue" statement allows the code to the next iteration of the loop without finishing the execution of all statements in a code block
	// The "break" statement is used to terminate the current loop once a match has been found

		for(let count = 0; count <= 20; count++){

			// if the remainder is zero
			if(count % 2 === 0){
				// Tells the code to continue to the next iteration of the loop;
				// This ignores all the sattements located after the continue statement;
				continue;
			};

			// The current value of number is printed out if the remainder is not equal to 0
			console.log('Continue and Break: ' + count);

			// If the current value of the count is greatyer than 10
			if(count > 10){

				// Tells the code to terminate/stop teh loop even if the expression/condition of the loop defines that it should execute so long as the value of count is less than or equal yo 20;
				// number value after 10 will no longer be printed 
				break;
			};
		};

	// Another Example

		let name = "alexandro";

		for(let i = 0; i < name.length; i++){
			console.log(name[i]);

			// If the vowel is equal to a, continue to thge next iteratyion of the loop
			if(name[i].toLowerCase() === 'a'){
				console.log("Continue to the next iteration");
				continue;
			};

			// If the current lwetter is equal to d, stop the loop
			if(name[i] == 'd'){
				break;
			};
		}