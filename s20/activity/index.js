function printDivisibleByFive(number) {
  console.log("The number you provided is: " + number);
  for (let i = number; i >= 0; i--) {
    if (i < 50) {
      console.log("The entered value is less than 50. Terminating the loop.");
      break;
    };
    if (i = 50) {
      console.log("The current value is at 50. Terminating the loop.");
      break;
    };
    if (i % 10 === 0) {
      console.log("The number is divisible by 10. Skipping the number.");
      continue;
    };
    if (i % 5 === 0) {
      console.log(i);
    };
  };
};

let number = prompt("Enter a number:");
printDivisibleByFive(number);



function removeVowels(str) {
  let newStr = '';
  for (let i = 0; i < str.length; i++) {
      if (str[i].toLowerCase() === 'a' || 
        str[i].toLowerCase() === 'e' || 
        str[i].toLowerCase() === 'i' || 
        str[i].toLowerCase() === 'o' || 
        str[i].toLowerCase() === 'u')
        continue;
      else
        newStr += str[i];
  };
  return newStr;
};

let str = prompt("Enter a string:");
console.log("String Entered: " + str);
let newStr = removeVowels(str);
console.log("New String: " + newStr);