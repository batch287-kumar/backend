/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/

	function printWelcomeMessage(){
		let fullName = prompt("Enter your Full Name");
		let age = prompt("Enter your age");
		let location = prompt("Enter your location");

		console.log("Hello, " + fullName + "!");
		console.log("Age: " + age);
		console.log("Location: " + location);
	}

	printWelcomeMessage();

	
	//first function here:

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	function displayFavMusicians(){
		let musician1 = "The vamps";
		let musician2 = "The Chainsmokers";
		let musician3 = "Demi Lovato";
		let musician4 = "Enrique Iglesias";
		let musician5 = "Charlie Puth";

		console.log("1. " + musician1);
		console.log("2. " + musician2);
		console.log("3. " + musician3);
		console.log("4. " + musician4);
		console.log("5. " + musician5);
	}
	displayFavMusicians();

	//second function here:

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	function displayFavMovies(){
		let movie1 = "John Wick Parabellum";
		let movie2 = "To All The Boys: Always and Forever";
		let movie3 = "Love, Simon ";
		let movie4 = "Papertowns";
		let movie5 = "The Half Of It";

		console.log("1. " + movie1);
		console.log("Rotten Tomato Ratings: 89%")
		console.log("2. " + movie2);
		console.log("Rotten Tomato Ratings: 79%")
		console.log("3. " + movie3);
		console.log("Rotten Tomato Ratings: 92%")
		console.log("4. " + movie4);
		console.log("Rotten Tomato Ratings: 58%")
		console.log("5. " + movie5);
		console.log("Rotten Tomato Ratings: 97%")
	}
	displayFavMovies();
	//third function here:

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

// printUsers();
let printFriends = function(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};
printFriends();