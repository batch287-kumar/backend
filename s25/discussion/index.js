console.log("Hello World");

// [ SECTION ] JSON Objects

	// JSON stand for JavaScript Object Notation
	// Syntax:
		/*
		{
			"propertyA": "valueA"
			"propertyB": "valueB"
		}
		*/
	
	// JSON Objects
	/*
		{
			"city": "Manila",
			"province": "Metro Manila"
			"country": "Philippines"
		}
	*/

// [ SECTION ] JSON Arrays

	/*
		"cities": [
			{
				"city": "Manila City", 
				"province": "Metro Manila", 
				"country": "Philippines"
			},
		{"city": "Quezon City", "province": "Metro Manila", "country": "Philippines"},
		{"city": "Makati City", "province": "Metro Manila", "country": "Philippines"}
		]
	*/

// [ SECTION ] JSON Methods
	// The JSON Objects contains methods for parsing and converting data into stringified JSON

	// Converting Data into Stringified JSON
		// Stringified JSON is a javascript object converted into a string to be used in other function of a Javascript application

	let batchesArr = [{ batchName: 'Batch X' }, { batchName: 'Batch V' }];

	// The "stringify" method is used to convert Javascript Object into a string

	console.log("Result from stringify method: ");
	console.log(batchesArr);
	console.log(JSON.stringify(batchesArr));

	let data = JSON.stringify({
		name: "John",
		age: 31,
		address: {
			city: 'Manila',
			country: 'Philippines'
		}
	});

	console.log(data);

// [ SECTION ] Using Stringify Method with Variables
	// When information is stored in a variable and is not hard coded into an object that is being stringified, we can supply the value with a variable 

	// Syntax
	/*
		JSON.stringify({
			propertyA: "VariableA",
			propertyB: "VariableB",
		})
	*/
	// Since we do not have a frontend application yet, we will use the prompt method in order to gather user data to be supplied to the user details

	// User details
		let firstName = prompt("What is your first name?");
		let lastName = prompt("What is your last name?");
		let age = prompt("What is your age?");
		let address = {
			city: prompt("Which city do you live in?"),
			country: prompt("Which country does your city address belong to?")
		};

		let otherData = JSON.stringify({
			firstName: firstName,
			lastName: lastName,
			age: age,
			address: address
		});

		console.log(otherData);

// [ SECTION ] Converting stringified JSON into Javascript Objects

		// This happens both for sending infromation to a backend application and sending information back to a frontend application.

			let batchesJSON =`[{"batchName" : "Batch X" }, { "batchName": "Batch Y"} ]`;

			console.log(batchesJSON);
			console.log("Result from parse method");
			console.log(JSON.parse(batchesJSON));

			// MINI-ACTIVITY
			// Parse the following into a JavaScript Object

			let stringifiedObject = `{"name":"John","age":31,"address":{"city":"Manila","country":"Philippines"}}`

			console.log(JSON.parse(stringifiedObject))
			console.log(stringifiedObject)