db.fruits.insertMany([
  {
    name : "Apple",
    color : "Red",
    stock : 20,
    price: 40,
    supplier_id : 1,
    onSale : true,
    origin: [ "Philippines", "US" ]
  },

  {
    name : "Banana",
    color : "Yellow",
    stock : 15,
    price: 20,
    supplier_id : 2,
    onSale : true,
    origin: [ "Philippines", "Ecuador" ]
  },

  {
    name : "Kiwi",
    color : "Green",
    stock : 25,
    price: 50,
    supplier_id : 1,
    onSale : true,
    origin: [ "US", "China" ]
  },

  {
    name : "Mango",
    color : "Yellow",
    stock : 10,
    price: 120,
    supplier_id : 2,
    onSale : false,
    origin: [ "Philippines", "India" ]
  }
]);

// [ SECTION ] MongoDB Aggregation
  // Used to generate manipulated data and perform operations to create filtered results that helps in analyzing data

  // Using the aggregate method
    // The "$match" is used to pass the documents that meet that specified conditions to the next pipeline stage/aggregation process.

  /*
      Syntax:
          { $match: { field: value } }

      The "$group" is used to group elements together and field-value pairs using the data from the grouped elements

      Syntax: 
          { $group: { _id: "value", fieldResult: "valueResult" } }

        Using both "$match" and group along with aggregation will find for products that are on sale and will group the total amount of stocks for all suppliers found.

        Syntax: 
            db.collectionName.aggregate([
                  { $match: { fieldA, valueA } },
                  { $group: { _id: "$fieldB" }, { result: { operation } } }
            ]);
  */

        // The "$" symbol will refer to a field name that is available in the documents that are being aggregated on.
        // The "$sum" operator will total te values of all "stock" fields in the returned documents that are found using the "$match" criteria.

  db.fruits.aggregate([
        { $match: { onSale: true}},
        { $group: { _id: "$supplier_id", total: { $sum: "$stock" } } }
  ]);

  // Field Projection with aggregation

  // The "$project" can be used when aggregating data to include/exclude fields from the returned results
  // The 0, means excluded and 1, means included

  /*
        Syntax:
              { $project: { field: 1/0 } }
  */

  db.fruits.aggregate([
          { $match: { onSale: true } },
          { $group: { _id: "$supplier_id", total: { $sum: "$stock" } } },
          { $project: {_id: 0} }
    ]);

  // Sorting aggregated results

      // The "$sort" can be used to change teh order of aggregated results
  // The -1, means that the aggregated result is in a reverse order and 1, means that it is in a ascending order

      /*
            Syntax:
                  { $sort { field: 1/-1 } }
      */

      db.fruits.aggregate([
              { $match: { onSale: true } },
              { $group: { _id: "$supplier_id", total: { $sum: "$stock" } } }
              { $sort: { total: -1 } }
        ]);

  // Aggregating results Based on Array Fields
      // The "$unwind" deconstruct an array field from a collection/field with an array value to output a result for each element.

      /*
          Syntax:
              { $nwind: field }
      */

          db.fruits.aggregate([
              { $unwind: "$origin" }
          ]);

  // Output should be: It displays fruit documents by their origin and the kinds of fruits that are supplied

      db.fruits.aggregate([
          { $unwind: "$origin" }, 
          { $group: { _id: "$origin", fruits: { $push: "$name" } } }
      ]);

      db.fruits.aggregate([
          { $unwind: "$origin" }, 
          { $group: { _id: "$origin", kinds: { $sum: 1 } } }
      ]);
