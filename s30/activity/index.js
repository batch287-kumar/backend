db.fruits.aggregate([
		{ $match: { onSale: true } },
		{ $group: { _id: "supplier_id", fruitsOnSale: { $sum: 1 } } },
        { $project: { _id: 0 } }
	]);

db.fruits.aggregate([
		{ $match: { stock : {$gte : 20} }},
		{ $group: { _id: "supplier_id", enoughStock: { $sum: 1 } } },
        { $project: { _id: 0 } }
	]);

db.fruits.aggregate([
		{ $group: { _id: "$supplier_id", avg_price: { $avg: "$price" } } },
		{ $project: { _id: 1, avg_price: 1 } }
	]);

db.fruits.aggregate([
		{ $group: { _id: "$supplier_id", max_price: { $max: "$price" } } },
		{ $project: { _id: 1, max_price: 1 } }
	]);


db.fruits.aggregate([
		{ $group: { _id: "$supplier_id", min_price: { $min: "$price" } } },
		{ $project: { _id: 1, min_price: 1 } }
	]);