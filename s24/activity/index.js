let num = 2;
let getCube = Math.pow(num,3);
console.log(`The cube of ${num} is ${getCube}`);

let addressArray = [ "258 Washington", "Ave NW", "California", 90011];

let [ avenue, city, country, pincode ] = addressArray;

console.log(`I live at ${avenue} ${city}, ${country} ${pincode}`);





let animal = {
	name: "RockLee",
    species: "Lion",
    weight: "200kg",
    length: "9 ft 4 in",
    color: "Yellow-Brown",
    age: "9 yrs"
};

let { name, species, weight, length, color, age } = animal;

console.log(`${name} is a ${species} in the african evergreen forests. it weights ${weight} and has a length of ${length}. It has a color of ${color} and he has lived a total of ${age} and counting.`);





let array_number = [1, 2, 3, 4, 5, 6, 7, 8, 9];

// array_number.forEach((number) => {
// 	console.log(number);
// });

// let sum = 0;
// array_number.forEach((number) => {
// 	sum += number;
// });
// console.log(sum);

let add = (array_number) => {
	let sum = 0;
	array_number.forEach((number) => {
		console.log(number);
		sum += number;
	});
	return sum;
}

let total = add(array_number);
console.log(total);





class dog {
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	};
};
const myDog = new dog('Frankie', 5, "Miniature Dachshund");
console.log(myDog);
