const User = require("../models/User");
const Course = require("../models/Course");
const auth = require("../auth");
const bcrypt = require("bcrypt");

module.exports.checkEmailExists = async (reqBody) => {
    try {
        const result = await User.find({ email: reqBody.email });

        if (result.length > 0) {
            return true;
        } else {
            return false;
        }
    } catch (error) {
        console.error('Error checking email:', error);
        throw error;
    }
};


module.exports.registerUser = (reqBody) => {

	console.log(reqBody);

	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		password: bcrypt.hashSync(reqBody.password, 10)
	});

	return newUser.save().then((user, error) => {
		if(error){
			return false
		} else {
			return true
		}
	})
}


module.exports.loginUser = (reqBody) => {
	return User.findOne({ email: reqBody.email }).then(result => {

		// Email doesn't exist
		if(result == null){
			return "false";
		} else {

			// We now know that the email is correct, is password correct also ?
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			// Correct password
			if(isPasswordCorrect){
				return { access: auth.createAccessToken(result) }

			// Password incorrect
			} else {
				return false
			};
		};
	});
};

module.exports.getProfile = (data) => {

	console.log(data); // Will return the key value pair of userId: data.Id

	return User.findOne({ id: data._id }).then(result => {
		if(result == null){
			return false;
		} else {
			result.password = "";
			return result;
		};
	});
};

module.exports.enroll = async (data) => {

	// let await update first the user models
	let isUserUpdated = await User.findById(data.userId).then(user => {
		user.enrollments.push({ courseId: data.courseId });

		return user.save().then((user, error) => {
			if(error){
				return false;
			} else {
				return true;
			};
		});
	});

	let isCourseUpdated = await Course.findById(data.courseId).then(course => {
		course.enrollees.push({ userId: data.userId });

		return course.save().then((course, error) => {
			if(error){
				return false;
			} else {
				return true;
			};
		});
	});

	if(isUserUpdated && isCourseUpdated){
		return true;
	} else {
		return false;
	};
};

