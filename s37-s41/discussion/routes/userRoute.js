const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");
const auth = require("../auth");

router.post("/checkEmail", async (req, res) => {
    try {
        const emailExists = await userController.checkEmailExists(req.body);
        res.status(200).json({ emailExists });
    } catch (error) {
        console.error('Error checking email:', error);
        res.status(500).json({ error: 'An error occurred while checking email.' });
    }
});


router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController	=> res.send(resultFromController));
});

router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(
		resultFromController));
});

router.get("/details", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	console.log(userData) // Will return the token.payload

	userController.getProfile({userId: userData.id }).then(resultFromController => res.send(
		resultFromController));
});

// Route for enroll user to a course
router.post("/enroll", auth.verify, (req, res) => {

	let data = {
		userId: auth.decode(req.headers.authorization).id,
		courseId: req.body.courseId 
	};

	userController.enroll(data).then(resultFromController => res.send(resultFromController));
});

module.exports = router;