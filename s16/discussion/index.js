console.log("Hello World!");

//[ SECTION ] Arithmetic Operators

	let x = 5;
	let y = 28;

	console.log(x);
	console.log(y);

	let sum = x + y;
	console.log("Result of addition operator: " + sum);

	let difference = x - y;
	console.log("Result of subtraction operator: " + difference);

	let product = x * y;
	console.log("Result of product operator: " + product);

	let quotient = x / y;
	console.log("Result of division operator: " + quotient);

	let remainder = y % x;
	console.log("Result of modulo operator: " + remainder);

//[ SECTION ] Assignment Operators

	// Basic Assignmenty Operator (=)
	// This assignment operator assigns the value of the right operand to a variable and assigns the result to the variable
	let assignmentNumber = 8;

	//Addition assignment operator
	//Addition assignment operator adds the value of the right operator to a variable and assigns the result to the variable.

	assignmentNumber = assignmentNumber + 2;
	console.log("Result of the addition operator: " + assignmentNumber);

	// Shorthand for assignmentNumber = assignmentNumber + 2 
	assignmentNumber += 2;
	console.log("Result of the shorthand addition operator: " + assignmentNumber);

	// Subtraction/Multiplication/Division Assignment Operator (-=, *=, /=)

	assignmentNumber -= 2;
	console.log("The result of subtraction assignment operator is " + assignmentNumber);

	assignmentNumber *= 2;
	console.log("The result of multiplication assignment operator is " + assignmentNumber);

	assignmentNumber /= 2;
	console.log("The result of division assignment operator is " + assignmentNumber);

// Multiple Operators and Praentheses

	//PENDAS Rule (Parenthesis, Exponents, Multiplication, Division, Addition and Subtraction)

	let mdas = 1 + 2 - 3 * 4 / 5;
	console.log("Result of mdas operation: " + mdas);

		// The operation follows:
		// 1-> 3 * 4 = 12
		// 2-> 12 / 5 = 2.4 
		// 3-> 1 + 2 = 3 
		// 4-> 3 - 2.4 = 0.6 

	let pemdas = 1 + (2 - 3) * (4 / 5);
	console.log("Result of pemdas operation: " + pemdas);

		// The operation follows:
		// 1-> (2 - 3 = -1) & (4 / 5 = 0.8)
		// 2-> 1 + (-1) * (0.8) = 1 + (-0.8)
		// 3-> 1 + (-0.8) = 0.2

//[ SECTION ] Increment and Decrement
	// Operators that adds or subtract values by 1 and reassigns the value of the variable where the increment/decrement was applied to

	let z = 1;

	let increment = ++z;
	console.log("Result of pre-increment: " + increment);
	console.log("Result of pre-increment: " + z);

	increment = z++;
	// The value of "z" was 2 before it was incremented
	console.log("Result of post-increment: " + increment);
	// The value of "z" was increased again reassigning the value to 3
	console.log("Result of post-increment: " + z);

	let decrement = --z;
	console.log("Result of pre-decrement: " + decrement);
	console.log("Result of pre-decrement: " + z);

	decrement = z--;
	// The value of "z" was 2 before it was decremented
	console.log("Result of post-decrement: " + decrement);
	// The value of "z" was decreased again reassigning the value to 1
	console.log("Result of post-decrement: " + z);

// [ SECTION ] Type Coercion
	// Is the automatic or implicit conversion of values from one data type to another.

	let numA = 10;
	let numB = 12;

	let coercion = numA + numB;
	console.log(coercion);
	console.log(typeof numA);
	console.log(typeof coercion);

	// Black text means that the output returned is a string data type.

	let numC = 16;
	let numD = 14;

	let nonCoercion = numC + numD;
	console.log(nonCoercion);
	console.log(typeof nonCoercion);

	let numE = true + 1;
	console.log(numE);
	// The boolean "true" is also associated with the value of 1.

	let numF = false + 1;
	console.log(numF);
	// The boolean "false" is also associated with the value of 0.

// [ SECTION ] Comparison Operator

	let juan = 'juan';

	// Equality Operator
	/* 
		-Checks whether the operands are equal/have the same content
		-Accepts to CONVERT and COMPARE
		-Returns a boolean value
	*/
	console.log("Equality Operators:")
	console.log(1 == 1);
	console.log(1 == 2);
	console.log(1 == '1');
	console.log(0 == false);
	console.log('juan' == juan);
	console.log('juan' == 'juan');

	// Inequality Operator (!=)
	/*
		- Checcks whether the operands are not equal/have different content
		- Attempts to COVERT and COMPARE operands of different data types
	*/
	console.log("Inequality Operators:")
	console.log(1 != 1);
	console.log(1 != 2);
	console.log(1 != '1');
	console.log(0 != false);
	console.log('juan' != juan);
	console.log('juan' != 'juan');

	// Strict Equality Operator (===)
	/* 
		- Check whether the operands are equal/have the same content
		- Also COMPARES the data types of 2 values
		- Strict Equality Operators are better to use in most cases to ensure that the data types are provided are correct
	*/	

	console.log("Strict Equality Operators:")
	console.log(1 === 1);
	console.log(1 === 2);
	console.log(1 === '1');
	console.log(0 === false);
	console.log('juan' === juan);
	console.log('juan' === 'juan');

	// Strict Inequality Operator (!==)
	/* 
		- Check whether the operands are not equal/have the same content
		- Also COMPARES the data types of 2 values
		- Strict Inequality Operators are better to use in most cases to ensure that the data types are provided are correct
	*/	

	console.log("Strict Inequality Operators:")
	console.log(1 !== 1);
	console.log(1 !== 2);
	console.log(1 !== '1');
	console.log(0 !== false);
	console.log('juan' !== juan);
	console.log('juan' !== 'juan');
// [ SECTION ] Relational Operators
	// Some comparison Operators check whether one value is greater than or less than the other value

	let a = 50;
	let b = 65;
	console.log("Relational Operators: ")
	// GT or Greater Than Operator ( > )
	let isGreaterThan = a > b;
	console.log(isGreaterThan);
	// LT or less than operator ( < )
	let isLessThan = a < b;
	console.log(isLessThan);
	// GTE or Greater Than Or Equal Operator ( >= )
	let isGTorEqual = a >= b;
	console.log(isGTorEqual);
	// LTE or Less Than Or Equal Operator ( <= )
	let isLTorEqual = a <= b;
	console.log(isLTorEqual);

// Logical Operator

	let isLegalAge = true;
	let isRegistered = false;

	// Logical AND operator (&& - Double Ampersand)
	//Returns true if all operands are true
	let allRequirementsMet = isLegalAge && isRegistered;
	console.log("Result of Logical AND Operator: " + allRequirementsMet);

	// Logical OR operator (|| - Double Pipe)
	//Returns true if one of the operands are true
	let someRequirementsMet = isLegalAge || isRegistered;
	console.log("Result of Logical OR Operator: " + someRequirementsMet);

	// Logical NOT operator (! - Exclaimation Point)
	//Returns the opposite value
	let someRequirementsNotMet = !isRegistered;
	console.log("Result of Logical NOT Operator: " + someRequirementsNotMet);