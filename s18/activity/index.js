/*
	
	1.  Create a function which will be able to add two numbers.
		-Numbers must be provided as arguments.
		-Display the result of the addition in our console.
		-function should only display result. It should not return anything.

		Create a function which will be able to subtract two numbers.
		-Numbers must be provided as arguments.
		-Display the result of subtraction in our console.
		-function should only display result. It should not return anything.

		-invoke and pass 2 arguments to the addition function
		-invoke and pass 2 arguments to the subtraction function
*/

	function addTwoNumbers(a,b){
		console.log("Displayed the sum of " + a + " and " + b);
		let c = a + b;
		console.log(c);
	};
	addTwoNumbers(5,15);

	function subtractTwoNumbers(a,b){
		console.log("Displayed the difference of " + a + " and " + b);
		let c = a - b;
		console.log(c);
	};
	subtractTwoNumbers(50,10);

/*	2.  Create a function which will be able to multiply two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the multiplication.

		Create a function which will be able to divide two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the division.

	 	Create a global variable called outside of the function called product.
			-This product variable should be able to receive and store the result of multiplication function.
		Create a global variable called outside of the function called quotient.
			-This quotient variable should be able to receive and store the result of division function.

		Log the value of product variable in the console.
		Log the value of quotient variable in the console.
*/

	let a = 50;
	let b = 10;
	function multiplyTwoNumbers(a,b){
		return a * b;
	};
	product = multiplyTwoNumbers(a,b);
	console.log("The product of " + a + " and " + b + ":");
	console.log(product);

	function quotientTwoNumbers(a,b){
		return a / b;
	};
	quotient = quotientTwoNumbers(a,b);
	console.log("The quotient of " + a + " and " + b + ":");
	console.log(quotient);

/*
	3. 	Create a function which will be able to get total area of a circle from a provided 		radius.
			-a number should be provided as an argument.
			-look up the formula for calculating the area of a circle with a provided/given radius.
			-look up the use of the exponent operator.
			-you can save the value of the calculation in a variable.
			-return the result of the area calculation.

		Create a global variable called outside of the function called circleArea.
			-This variable should be able to receive and store the result of the circle area calculation.

	Log the value of the circleArea variable in the console.
*/

	function calculateCircleArea(radius) {
	  if (radius <= 0) {
	    return "Radius must be a positive number.";
	  }
	  return Math.PI * Math.pow(radius, 2); 
	}

	var radius = 15;
	var circleArea = calculateCircleArea(radius);
	console.log("The result of getting the area of a circle with " + radius + " radius is:");
	console.log(circleArea);


/*
	4. 	Create a function which will be able to get total average of four numbers.
			-4 numbers should be provided as an argument.
			-look up the formula for calculating the average of numbers.
			-you can save the value of the calculation in a variable.
			-return the result of the average calculation.

	    Create a global variable called outside of the function called averageVar.
			-This variable should be able to receive and store the result of the average calculation
			-Log the value of the averageVar variable in the console.
*/

	function averageOfNumbers(a, b, c, d){
		return (a + b + c + d) / 4;
	};
	let p = 20, q = 40, r = 60, s = 80;
	let averageVar = averageOfNumbers(p, q, r, s);
	console.log("The average of " + p + ", " + q + ", " + r + " and " + s + ":");
	console.log(averageVar);

/*
	5. Create a function which will be able to check if you passed by checking the percentage of your score against the passing percentage.
			-this function should take 2 numbers as an argument, your score and the total score.
			-First, get the percentage of your score against the total. You can look up the formula to get percentage.
			-Using a relational operator, check if your score percentage is greater than 75, the passing percentage. Save the value of the comparison in a variable called isPassed.
			-return the value of the variable isPassed.
			-This function should return a boolean.

		Create a global variable called outside of the function called isPassingScore.
			-This variable should be able to receive and store the boolean result of the checker function.
			-Log the value of the isPassingScore variable in the console.
*/

	function isPassed(myScore, fullMarks){
		return (myScore/fullMarks) >= 0.75;
	};
	let myScore = 38;
	let fullMarks = 50;
	let isPassingScore = isPassed(myScore, fullMarks);
	console.log("Is " + myScore + "/" + fullMarks + " a passing score?");
	console.log(isPassingScore);
