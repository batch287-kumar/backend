console.log("Hellow World");

// [ SECTION ] Functions

	// [ SUB-SECTION ] Parameters and Arguments

	// Functions in JS are lines/blocks of codes that tell our device/application/browser to perform a certain task when called/invoked/triggered

	function printInput(){
		let nickname = prompt("Enter your nickname:");
		console.log("Hi, " + nickname);
	};

	//printInput();

	// Fot other cases, function can also process data directly passed into it instead of relying on global variable and prompt()

	// name123456 ONLY ACTS LIKE A VARIABLE
	function printName(name123456){ // name123456 is the parameter
		// This one should work as well
			// let name = name123456;
			// console.log("Hi, " + name);

		console.log("Hi, " + name123456);
	};

		// console.log(name123456); // This one is error as it is not defined

	printName("Juana"); // "Juana" is argument.

	// You can directly pass data into the function. The function can then call/use data which is referred as "name123456" within the function

	printName("Carlo");

	// When a "printName()" function is called again, it stores the value of "Carlo" in the parameter "name123456" then uses it to print a message.

	// Variables can also be passed as an argument.
	let sampleVariable = "Yui";

	printName(sampleVariable);

	// Function arguments cannot be used by a function if there are no parameters provided within the function.

	printName(); // It will be Hi, undefined

		function checkDivisibilityBy8(num){
			let remainder = num % 8;
			console.log("The remainder of " + num + " divided by 8 is " + remainder);
			let isDivisibleBy8 = remainder === 0;
			console.log("Is " + num + " divisible by 8?");
			console.log(isDivisibleBy8);
		};

		checkDivisibilityBy8(8);
		checkDivisibilityBy8(52);

		// You can also do the same using prompt(), however, take note that prompt() outputs a string which is not ideal for mathematical computations.

	// Functions as arguments

		// Function parameters also accepts other functions as arguments

		function argumentFunction(){
			console.log("This function was passed as argument before the message was printed");
		};

		// function invokeFunction(argumentFunction){
		// 	argumentFunction();
		// };

		function invokeFunction(iAmNotRelated){
			// console.log(iAmNotRelated);
			iAmNotRelated();
			argumentFunction();
		};

		invokeFunction(argumentFunction);

		// Adding and removing the paranthesis "()" impacts the output of JS heavily.

		// console.log(argumentFunction);
		// Will provide information about a function in the console using console.log();

	// Using multiple parameters

		// Multiple "arguments" will correspond to the number of "parameters" declared in a function in succeding order.

		function createFullName(firstName, middleName, lastName){
			console.log(firstName + " " + middleName + " " + lastName);

			console.log(middleName + " " + lastName + " " + firstName);
		};
		
		createFullName("Mike", "Kel", "Jordan");

		createFullName("Mike", "Jordan");

		createFullName("Mike", "Kel", "Jordan", "Mike");

		// In JS, providing more/less arguments than the expected parameter will not return an error.

		// Using Variable as Arguments

		let firstName = "Dwayne";
		let secondName = "The Rock";
		let lastName = "Johnson";

		createFullName(firstName, secondName, lastName);

// [ SECTION ] The Return Statement

		// The return statement allows us to output a value from a function to be passed to the line/block of code that invokeed/called the function.

			function returnFullName(firstName, middleName, lastName){
				return firstName + " " + middleName + " " + lastName;
				console.log("This message is from console.log");
				console.log("This message is from console.log");
				console.log("This message is from console.log");
			};

			let completeName = returnFullName("Jeffrey", "Smith", "Bezos");
			console.log(completeName);

			// In our example, the "returnFullName" function was invoked called in the same line as declaring a variable.

			// Whatever value is stored is returned from the "returnFullName" function is stored in the "completeName" variable.

			// Notice that the console.log() after the return is no longer printed in the console, that is because ideally any line/block of code that comes after the return statement is ignored because it ends the function execution.

			//In this example, console.log() will print the returned value of the returnFullName() function.
			console.log(returnFullName(firstName, secondName, lastName));

			//You can also create a variable to contain the result and return that variable instead.

			function returnAddress(city, country){
				let fullAddress = city + ", " + country;
				return fullAddress;
			}

			let myAddress = returnAddress("Manilla City", "Philippines");
			console.log(myAddress);

			// On the other hand, when a function only has console.log() to display its result will be undefined instead

			function printPlayerInfo(username, level, job){
				console.log("Username: " + username);
				console.log("Level: " + level);
				console.log("Job: " + job);
			};

			let user1 = printPlayerInfo("knight_white", 95, "Paladin");
			console.log(user1);

			// Returns undefined because printPlayerInfo return nothing. It only console.log the details.

			// You cannot save any value from printPlayerInfo() because it does not return anything.

