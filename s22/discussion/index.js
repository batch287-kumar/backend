console.log("Hello World");

// Array methods 

	// JS has built-in functions and methods for arrays. This allows us to manipulate and access array items.

	// Mutators Methods

		// Mutator mwethods are functions that "mutate" or change an array after they're created 

	let fruits = [ 'Apple', 'Orange', 'Kiwi', 'Dragon Fruit' ];

	// push()
		// Adds an element in the end of an array AND returns the array's length 
		// Syntax :
			// arrayName.push();

	console.log('Current array:');
	console.log(fruits);
	let fruitsLength = fruits.push('Mango');
	console.log(fruitsLength);
	console.log('Mutated array from push method:');
	console.log(fruits);

	fruits.push('Avocado', 'Guava');
	console.log('Mutated array from push method:');
	console.log(fruits);

	// pop()
		// Removes the last element in an array AND returns the removed element
		// Syntax:
			// arrayName.pop()

	let removeFruit = fruits.pop();
	console.log(removeFruit);
	console.log("Mutated array from pop method:")
	console.log("fruits");

	// unshift()
		// Adds one or more element at the beginning of an array
		// Syntax:
			// arrayName.unshift('elementA');
			// arrayName.unshift('elementA', 'elementB');

	let unshift = fruits.unshift('Lime', 'Banana');
	console.log(unshift);
	console.log("Mutated array from unshift method:");
	console.log(fruits);

	// shift()
		// Removes an element at the beginning of an array AND returns the removed element:
		// Syntax:
			// arrayName.shift();

	let anotherFruit = fruits.shift();
	console.log(anotherFruit);
	console.log('Mutated array from shift method:');
	console.log(fruits);

	// splice()
		// Simulktaneosly removes element from a specifies index number and adds elements 
		// Syntax:
			// arrayName.splice(startingIndex, deleteCount, deleteCount, elementToBeAdded);

	let splice = fruits.splice(1, 2, 'Lime', 'Cherry');
	console.log(splice);
	console.log('Mutated array from splice method:');
	console.log(fruits);

	// sort()
		// Rearranges the array in alphanumeric order
		// Syntax:
			// arrayName.sort();

	fruits.sort();
	console.log('Mutated array from sort method:');
	console.log(fruits);

	// reverse()
		// Reverse the order of array elements
		// Syntax:
			// arrayName.reverse();

	fruits.reverse();
	console.log('Mutated array from reverse method');
	console.log(fruits);

	// Non-mutators method

		// Non-Mutators methods are functions that do not modify or change an array after they're created 

	let countries = [ 'US', 'PH', 'CAN', 'SG', 'TH', 'PH', 'FR', 'IND' ];

	// indexOf()
		// Returns the index number of the first matching element found in an array
		// If no match was found, the result will be -1
		// Syntax:
			// arrayName.indexOf(searchValue);

	let firstIndex = countries.indexOf('PH');
	console.log('Result of indexOf method: ' + firstIndex);

	let invalidCountry = countries.indexOf('BR');
	console.log('Result of indexOfmethod: ' + invalidCountry);

	// lastIndexOf()
		// 	Returns the index number of the last matching element found in an array
	 	// Syntax:
	 		// arrayNmae.lastIndexOf(searchValue);
	 		// arrayNmae.lastIndexOf(searchValue, fromIndex);

	let lastIndex = countries.lastIndexOf('PH');
	console.log('Result of lastIndex method: ' + lastIndex);

	// Getting the index number starting from a specifies index
	let lastIndexStart = countries.lastIndexOf('PH', 4);
	console.log('Result of lastIndexStart method: ' + lastIndexStart);

	// slice()
		// Portions/slices elements from an array AND returns a new array
		// Syntax:
			// arrayName.slice(startingIndex);
			// arrayName.slice(startingIndex, endingIndex);

	console.log(countries);
	// Slicing off elements from a specified index to the last elements
	let slicedArrayA = countries.slice(2);
	console.log('Result from the slice method:');
	console.log(slicedArrayA);
	console.log(countries);

	// Slicing off elements from a specified index to another index 
	let slicedArrayB = countries.slice(2, 5);
	console.log('Result from slice method: ');
	console.log(slicedArrayB);

	// Slicing off elements starting from the last element of an array
	let slicedArrayC = countries.slice(-4);
	console.log('Result from slice method: ');
	console.log(slicedArrayC);

	// toString()
		// Returns an array as a string separated by commas
		// Syntax: 
			// arrayName.toString();

	let stringArray = countries.toString();
	console.log('Result from toString method:');
	console.log(stringArray);

	// concat()
		// Combines two arrays and returns the combined result
		// Syntax:
			// arrayA.concat(arrayB);

	let taskArrayA = ['drink HTML', 'eat Javascript'];
	let taskArrayB = ['inhale CSS', 'breath sass'];
	let taskArrayC = ['get git', 'be bootstrap'];

	let task = taskArrayA.concat(taskArrayB);
	console.log('Result from concat method:');
	console.log(task);

	// Combining multiple arrays
	console.log('Result from concat method:');
	let allTasks = taskArrayA.concat(taskArrayB, taskArrayC);
	console.log(allTasks);

	// Combine arrays with elements 
	let combinedTasks = taskArrayA.concat('smell express', 'throw react');
	console.log('Result from concat method:');
	console.log(combinedTasks);

	// join()
		// Returns an array as a string separated by specified separator string
		// Syntax: 
			// arrayName.join('separatedString');

	let users = ['John', 'Jane', 'Joe', 'Robert'];

	console.log(users.join());
	console.log(users.join(""));
	console.log(users.join(" - "));

	// Iterations Methods 
		// Iterations method are loops designed to perform repetitive task on arrays

	// forEach()
		// Similar to for loop that iterates on each array element
	// forEach() does not return anything
		// Syntax:
	 		// arrayNmae.forEach(function(indivElement){
	 		// 	statement;
	 		// });

	allTasks.forEach(function(task){
		console.log(task);
	});

	let filteredTasks = [];

	// Looping Through All Array Items
		// It's good practise to print the current element in the console when working with array iterations

	allTasks.forEach(function(task){
		console.log(task);

		if(task.length > 10){
			console.log(task);
			filteredTasks.push(task);
		};
	});

	console.log('Result of filtered task');
	console.log(filteredTasks);

	// map()
		// Iterates on each element AND returns new array with different values depending on the result of the function's operation
		// unlike the forEach method, the map method requires the use of "return" statementg in order to create another array with the performed operation
		// Syntax:
			// let/const resultArray = arrayName.map(function(indivElement))

	let numbers = [1, 2, 3, 4, 5];

	let numberMap = numbers.map(function(number){
		return number * number;
	});

	console.log("Original Array:");
	console.log(numbers);
	console.log("Result of map method:");
	console.log(numberMap);

	// map() vs. forEach()

	let numberForEach = numbers.forEach(function(number){
		return number * number;
	});

	console.log(numberForEach); // undefined

	// forEach(), loops over all items in the array as does map(), but forEach() does not return a new array.

	// every()
		// Checks if all elements in an array meet the given condition
		// Returns a true value if all elements meet the condition and false if otherwise
		// Syntax: 
			// let/const resultArray = arrayName.every(function(indivElem){
				// return expression/condition;
			// })

	let allValid = numbers.every(function(number){
		return (number < 3);
	});

	console.log("Result of every method:");
	console.log(allValid);

	// some()
		// Checks if at least one element in the array meets the condition
		// Syntax:
			// let/const resultArray = arrayName.some(function(indivEle){
				// return expression/condition;
			// })

	let someValid = numbers.some(function(number){
		return (number < 2);
	});

	console.log("Result of some method:");
	console.log(someValid);

	if(someValid){
		console.log('Some numbers in the array are greater than 2');
	};

	// filter()
		// Returns a new array that contains elements which meets teh given condition
		// Syntax:
			// let const resultArray = arrayName.filter(function(indivEle){
				// return expression/condition;
			// });

	let filterValid = numbers.filter(function(number){
		return (number < 3);
	});

	console.log('Result of the filter method: ');
	console.log(filterValid);

	let nothingFound = numbers.filter(function(number){
		return (number = 0);
	});

	console.log('Result of the filter method: ');
	console.log(nothingFound);

	// Filtering using forEach()

	let filteredNumbers = [];

	numbers.forEach(function(number){
		console.log(number);

		if(number < 3){
			filteredNumbers.push(number);
		};
	});

	console.log('Result of filter method: ');
	console.log(filteredNumbers);

	let products = [ 'Mouse', 'Keyboard', 'Laptop', 'Monitor' ];

	// includes()
		// include() method checks that teh argument passed can be found in the array
		// Syntax:
			// arrayName.includes(<argumentToFind>);

	let productFound1 = products.includes('Mouse');
	console.log(productFound1); // true 

	productFound1 = products.includes('Headset');
	console.log(productFound1); // false

	// Chaining Methods
		// The result of the first method is used on the second method untill all 'chained'

	let filteredProducts = products.filter(function(product){
		return product.toLowerCase().includes('a');
	});

	console.log(filteredProducts);

	// reduce()
		// Evaluates elements from left to right and returns/reduce the array into a single value
		// Syntax:
			// let/const resultArray = arrayName.reduce(function(accumulator, currentValue){
				// return expression/condition;
			// })

		// How the 'reduce' method works
			// 1. The first/result element in the array is stored in the 'currentValue' parameter
			// 2. The second/next element in the array is stored in the 'currentValue' parameter
	 		// 3. An operation is perfomed on the two elements
			// 4. The loop repeats step 1-3 until all element have been worked on
 

	let iteration = 0;
	console.log(numbers);

	let reducedArray = numbers.reduce(function(x, y){
		console.warn('Current Iteration: ' + ++iteration);
		console.log('accumulator: ' + x );
		console.log('currentValue: ' + y );

		return x + y;
	
	});

	console.log('Result of reduce method: ' + reducedArray);

	// Reducing String Arrays
	let list = [ 'Hellow', 'Again', 'Wurld' ];

	let reducedJoin = list.reduce(function(x,y){
		return x + ' ' + y;
	});

	console.log('Result of reduce method: ' + reducedJoin);