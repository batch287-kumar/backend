const Task = require("../models/task");

module.exports.getTask = (taskId) => {
	return Task.findById(taskId).then((result, err) => {
		if(err){
			console.log(err)
			return false
		} else {
			return result
		};
	});
};


module.exports.markTaskComplete = (taskId) => {
	return Task.findById(taskId).then((result, err) => {
		if(err){
			console.log(err)
			return false
		};
		result.status = "complete";
		return result.save().then((updatedStatus, saveErr) => {
			if(saveErr){
				console.log(saveErr)
				return false
			} else {
				return "Status updated."
			};
		});
	});
};