const express = require("express");
const router = express.Router();
const taskController = require("../controller/taskController")

router.get("/:id", (req, res) => {
	taskController.getTask(req.params.id).then(resultFromController => res.send(resultFromController));
});

router.put("/:id/complete", (req, res) => {
  taskController.markTaskComplete(req.params.id).then(resultFromController => res.send(resultFromController));
});

module.exports = router;